-- USE music_db

-- A
SELECT * FROM `artists_table` WHERE `artist_name` LIKE '%D%';

-- B
SELECT * FROM `songs_table` WHERE `song_length` < 230;

-- C
SELECT albums_table.album_name, songs_table.song_name, songs_table.song_length  FROM `albums_table` JOIN `songs_table` ON songs_table.id = albums_table.song_id;

-- D
SELECT * FROM `artist_table` JOIN `albums_table` ON albums_table.id = artist_table.album_id WHERE albums_table.album_name LIKE '%a%';

-- E
SELECT * FROM `albums_table` ORDER BY `album_name` DESC LIMIT 4

-- F
SELECT *  FROM `albums_table` JOIN `songs_table` ON songs_table.id = albums_table.song_id ORDER BY `album_name` DESC;